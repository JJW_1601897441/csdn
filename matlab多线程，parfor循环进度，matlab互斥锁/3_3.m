% 创建一个 DataQueue 对象
dq1 = parallel.pool.DataQueue;
dq2 = parallel.pool.DataQueue;
file1 = fopen('ccc1.txt','w');
file2 = fopen('ccc2.txt','w');
% 多重匿名函数
fun_with_params = @(data) saveData(data, file1);
fun_with_params = @(data) saveData(data, file2);
% 在 DataQueue 上设置 afterEach 方法
afterEach(dq1, fun_with_params);
afterEach(dq2, fun_with_params);
% 在工作线程中定义处理函数并发送数据到 DataQueue
parfor i = 1:100
    % 替换成自己的
    a = rand();


    % 在这里通过 send 函数将数据发送到 DataQueue
    % 可以发送单个，也可也发送数组
    send(dq1, i);
    send(dq2, i);
end

fclose(file);

% 等待所有数据接收完成


% 显示接收到的数据
% 辅助函数用于保存接收到的数据到数组
function saveData(data, file)
    % 在此处编写后处理代码
    fprintf(file,'%.10f ', data); 
    fprintf(file,'\n'); 
end
