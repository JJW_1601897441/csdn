% 设置循环次数
N = 10000;
% 创建一个迭代计数器对象
parfor_progress(N);
parfor i = 1:N
    % pause替换乘自己的就可以了
    pause(rand)

    parfor_progress;
end
parfor_progress(0);
