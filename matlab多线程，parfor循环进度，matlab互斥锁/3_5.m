% 创建一个 DataQueue 对象
dq1 = parallel.pool.DataQueue;
file1 = fopen('ccc11.txt','w');
file2 = fopen('ccc21.txt','w');
file3 = fopen('ccc31.txt','w');
% 多重匿名函数
fun_with_params1 = @(data1) saveData(data1, file1, file2,file3);
% 在 DataQueue 上设置 afterEach 方法
afterEach(dq1, fun_with_params1);
% 在工作线程中定义处理函数并发送数据到 DataQueue
parfor i = 1:100
    % 替换成自己的
    a = rand();

    data = {i,i,i}
    % 在这里通过 send 函数将数据发送到 DataQueue
    % 可以发送单个，也可也发送数组
    send(dq1, data);
end


% 等待所有数据接收完成

fclose(file1);
fclose(file2);
fclose(file3);
% 显示接收到的数据
% 辅助函数用于保存接收到的数据到数组
function saveData(data, file1,file2,file3)
    % 在此处编写后处理代码
    data1 = data{1};
    data2 = data{2};
    data3 = data{1};
    fprintf(file1,'%.10f ',  data1); 
    fprintf(file1,'\n'); 
    fprintf(file2,'%.10f ',  data2);
    fprintf(file2,'\n'); 
    fprintf(file3,'%.10f ',  data3);
    fprintf(file3,'\n'); 
end

